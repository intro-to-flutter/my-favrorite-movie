import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/diagnostics.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My favorite movie",
      theme: ThemeData.dark(),
      home: Scaffold(
        appBar: AppBar(
            title: Center(child: Text("Lord of the Rings"))
        ),
        body: Center(
            child: Image.asset("images/lord_of_the_rings.jpg",)),
      ),
    );
  }
}
